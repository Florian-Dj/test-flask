FROM python:alpine

COPY . .

RUN pip3 install -r requirement.txt
EXPOSE 5000

ENTRYPOINT [ "python" ]
CMD [ "main.py" ]
